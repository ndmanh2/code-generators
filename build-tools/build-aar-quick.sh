#!/bin/sh

TRUEYOUR_PATH=~/Documents/trueyours
TMP_ANDROID_PATH=~/Documents/android
EDC_PATH=~/Documents/truemerchant-android

echo '[sync built files]'
rsync -a --delete $TRUEYOUR_PATH/platforms/android/assets/www/ $TMP_ANDROID_PATH/assets/www/

echo '[run build script]'
cd $TMP_ANDROID_PATH
./gradlew build

echo '[copy output files]'
cp ./build/outputs/aar/android-debug.aar $EDC_PATH/trueyou-android/release/aar/app-debug.aar
cp ./build/outputs/aar/android-release.aar $EDC_PATH/trueyou-android/release/aar/ManagerApp-release.aar

cp ./CordovaLib/build/outputs/aar/* $EDC_PATH/trueyou-android/release/CordovaLib/

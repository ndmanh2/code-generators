#!/bin/sh

TRUEYOUR_PATH=~/Documents/trueyours
TMP_ANDROID_PATH=~/Documents/android
EDC_PATH=~/Documents/truemerchant-android

ANDROID_HOME=~/Library/Android/sdk

echo '[sync project]'
rsync -a --delete $TRUEYOUR_PATH/platforms/android/ $TMP_ANDROID_PATH/

echo '[patch project]'
patch $TMP_ANDROID_PATH/AndroidManifest.xml app-manifest.patch
patch $TMP_ANDROID_PATH/build.gradle build-android.patch
patch $TMP_ANDROID_PATH/CordovaLib/build.gradle build-cordova.patch

echo '[run build script]'
cd $TMP_ANDROID_PATH
echo 'sdk.dir=/Users/nguyenduchieu/Library/Android/sdk' > local.properties
./gradlew build --refresh-dependencies

echo '[copy output files]'
cp ./build/outputs/aar/android-debug.aar $EDC_PATH/trueyou-android/release/aar/app-debug.aar
cp ./build/outputs/aar/android-release.aar $EDC_PATH/trueyou-android/release/aar/ManagerApp-release.aar

cp ./CordovaLib/build/outputs/aar/* $EDC_PATH/trueyou-android/release/CordovaLib/

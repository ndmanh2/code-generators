/**
 * Code generator for language file in Ionic
 */

let languages = {
  'Thai': '../trueyours/src/assets/i18n/th.json',
  'English': '../trueyours/src/assets/i18n/en.json',
};

let prompt = require('prompt-sync')();
let fs = require("fs");


let key = prompt('Enter string key:');

for (let lang in languages) {
  let data = JSON.parse(fs.readFileSync(languages[lang], 'utf8'));
  
  if (key) {
    data[key] = prompt("Input value for " + lang + ":");
    console.log("Added " + key + " for " + lang);
  }

  const ordered = {};
  
  Object.keys(data).sort().forEach(function(key) {
    ordered[key] = data[key];
  });

  fs.writeFile(languages[lang], JSON.stringify(ordered, null, 2), function(err) {
    if(err) {
      return console.log(err);
    }
  });
}

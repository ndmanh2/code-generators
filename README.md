# Code helper tools

### Jhipster-Ionic translation string generator
Script will add key and translation string for two (or more up to your config) languages and automatically sort the keys in alphabetic order.
**Step:**
`cd add-translation-string`
`npm install`
Check file path to Thai and English json on `addTranslationString.js` then run:
`node addTranslationString.js`
Follow the instruction.

### Build Ionic-generated android project as AAR
Script will copy Ionic-generated android project to another project and modify it as a library project, then build as AAR files and copy to native folder for integration
**Step:**
`cd build-tools`
Check file path on `build-aar.sh` then run:
`./build-aar.sh`
Done.

For lighter option, you can run:
`./build-aar-quick.sh`
The script will just sync build `www` files and folder and build aar files without refresh dependencies.
This script is helpful if you've just changed some portions of code without including new library.